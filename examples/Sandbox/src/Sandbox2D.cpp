#include "Sandbox2D.hpp"

#include <imgui.h>
#include <glm/gtc/type_ptr.hpp>

Sandbox2D::Sandbox2D()
    : Real::Layer("Sandbox2D"), camera_controller(1280.0f / 720.0f)
{}

void Sandbox2D::on_attach()
{
    checkerboard = Real::Texture2D::create("res/textures/Checkerboard.png");
}

void Sandbox2D::on_update(Real::Timestep timestep)
{
    camera_controller.on_update(timestep);

    Real::RenderCommand::set_clear_color({0.1f, 0.1f, 0.1f, 1.0f});
    Real::RenderCommand::clear();

    // Begin accepting rendering data.
    Real::Renderer2D::begin_scene(camera_controller.get_camera());
    Real::Renderer2D::draw_quad(
        {-1.0f, 0.0f}, {1.0f, 1.0f}, {0.8f, 0.2f, 0.3f, 1.0f});
    Real::Renderer2D::draw_quad(
        {0.5f, -0.5f}, {0.5f, 0.75f}, {0.3f, 0.2f, 0.8f, 1.0f});
    Real::Renderer2D::draw_quad(
        {0.0f, 0.0f, -0.1f}, {10.0f, 10.0f}, *checkerboard);
    Real::Renderer2D::end_scene(); // Render submitted data.
}

void Sandbox2D::on_imgui_render()
{
    ImGui::Begin("Settings");
    ImGui::ColorEdit4("Square Color", glm::value_ptr(square_color));
    ImGui::End();
}

void Sandbox2D::on_event(Real::Event& event)
{
    camera_controller.on_event(event);
}
