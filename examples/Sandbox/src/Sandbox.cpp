#include <Real.hpp>
#include <EntryPoint.hpp>

#include <memory>

#include <imgui.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Sandbox2D.hpp"

// class ExampleLayer : public Real::Layer {
//  private:
//    Real::ShaderLibrary shader_library;
//
//    std::shared_ptr<Real::VertexArray> vertex_array;
//    std::shared_ptr<Real::Shader> shader;
//
//    std::shared_ptr<Real::VertexArray> square_va;
//    std::shared_ptr<Real::Shader> flat_color_shader;
//
//    std::shared_ptr<Real::Texture2D> texture;
//    std::shared_ptr<Real::Texture2D> cherno_logo_texture;
//
//    glm::vec4 square_color = {0.2f, 0.3f, 0.8f, 1.0f};
//
//    Real::OrthographicCameraController camera_controller;
//
//  public:
//    ExampleLayer() : Layer("Example"), camera_controller(1280.0f / 720.0f)
//    {
//        // Triangle rendering.
//        vertex_array = Real::VertexArray::create();
//
//        float vertices[3 * 7] = {-0.5f, -0.5f, 0.0f, 0.8f, 0.2f, 0.8f, 1.0f,
//                                 0.5f,  -0.5f, 0.0f, 0.2f, 0.3f, 0.8f, 1.0f,
//                                 0.0f,  0.5f,  0.0f, 0.8f, 0.8f, 0.2f, 1.0f};
//
//        auto vertex_buffer =
//            Real::VertexBuffer::create(vertices, sizeof(vertices));
//
//        Real::BufferLayout layout = {{Real::ShaderDataType::Float3,
//        "position"},
//                                     {Real::ShaderDataType::Float4, "color"}};
//        vertex_buffer->set_layout(layout);
//
//        vertex_array->add_vertex_buffer(vertex_buffer);
//
//        uint32_t indices[3] = {0, 1, 2};
//        auto index_buffer   = Real::IndexBuffer::create(indices, 3);
//        vertex_array->set_index_buffer(index_buffer);
//
//        std::string vertex_src   = R"(
//                #version 330 core
//
//                layout(location = 0) in vec3 position;
//                layout(location = 1) in vec4 color;
//
//                uniform mat4 view_projection;
//                uniform mat4 transform;
//
//                out vec3 varying_position;
//                out vec4 varying_color;
//
//                void main()
//                {
//                    varying_position = position;
//                    varying_color = color;
//                    gl_Position = view_projection
//                                        * transform * vec4(position, 1.0);
//                }
//        )";
//        std::string fragment_src = R"(
//                #version 330 core
//
//                layout(location = 0) out vec4 color;
//
//                in vec3 varying_position;
//                in vec4 varying_color;
//
//                void main()
//                {
//                    color = vec4(varying_position * 0.5 + 0.5, 1.0);
//                    color = varying_color;
//                }
//        )";
//        shader = Real::Shader::create("VertexColor", vertex_src,
//        fragment_src);
//        /* ------------------------------------------------------------------
//        */
//
//        // Square rendering.
//        square_va                    = Real::VertexArray::create();
//        float square_vertices[5 * 4] = {
//            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.5f,  -0.5f, 0.0f, 1.0f, 0.0f,
//            0.5f,  0.5f,  0.0f, 1.0f, 1.0f, -0.5f, 0.5f,  0.0f, 0.0f, 1.0f};
//        auto square_vb = Real::VertexBuffer::create(
//            square_vertices, sizeof(square_vertices) / sizeof(float));
//        square_vb->set_layout({{Real::ShaderDataType::Float3, "position"},
//                               {Real::ShaderDataType::Float2, "tex_coord"}});
//        square_va->add_vertex_buffer(square_vb);
//
//        uint32_t square_indices[6] = {0, 1, 2, 2, 3, 0};
//        auto square_ib = Real::IndexBuffer::create(square_indices, 6);
//        square_va->set_index_buffer(square_ib);
//
//        flat_color_shader =
//        Real::Shader::create("res/shaders/FlatColor.glsl"); auto
//        texture_shader = shader_library.load("res/shaders/Texture.glsl");
//        texture = Real::Texture2D::create("res/textures/Checkerboard.png");
//        cherno_logo_texture =
//            Real::Texture2D::create("res/textures/ChernoLogo.png");
//
//        // Create a texture sampler at a texture slot zero.
//        shader->bind();
//        shader->set_uniform_int("texture_sampler", 0);
//    }
//
//    void on_update(Real::Timestep timestep) override
//    {
//        camera_controller.on_update(timestep);
//
//        Real::RenderCommand::set_clear_color({0.2f, 0.2f, 0.2f, 1.0f});
//        Real::RenderCommand::clear();
//
//        // Begin accepting rendering data.
//        Real::Renderer::begin_scene(camera_controller.get_camera());
//
//        // Calculate square transform and submit it for rendering.
//        shader->bind();
//        shader->set_uniform_float4("uniform_color", square_color);
//
//        static glm::mat4 shrink = glm::scale(glm::mat4(1.0f),
//        glm::vec3(0.1f)); for (int x = -10; x <= 10; x++) {
//            for (int y = -10; y <= 10; y++) {
//                glm::vec3 position(x * 0.11f, y * 0.11f, 0.0f);
//                glm::mat4 translation =
//                    glm::translate(glm::mat4(1.0f), position);
//                glm::mat4 transform = translation * shrink;
//                Real::Renderer::submit(flat_color_shader, square_va,
//                transform);
//            }
//        }
//
//        texture->bind();
//        static glm::mat4 enlarge = glm::scale(glm::mat4(1.0f),
//        glm::vec3(1.5f)); auto texture_shader      =
//        shader_library.get("Texture"); Real::Renderer::submit(texture_shader,
//        square_va, enlarge); cherno_logo_texture->bind();
//        Real::Renderer::submit(texture_shader, square_va, enlarge);
//
//        // Submit the triangle for rendering.
//        // Real::Renderer::submit(shader, vertex_array);
//
//        Real::Renderer::end_scene(); // Render submitted data.
//    }
//
//    void on_event(Real::Event& event) override
//    {
//        camera_controller.on_event(event);
//    }
//
//    void on_imgui_render() override
//    {
//        ImGui::Begin("Settings");
//        ImGui::ColorEdit4("Square Color", glm::value_ptr(square_color));
//        ImGui::End();
//    }
//};

class Sandbox : public Real::Application {
  public:
    Sandbox()
    {
        // Layer for other testing.
        // auto example_layer = std::make_unique<ExampleLayer>();
        // push_layer(std::move(example_layer));

        // Layer for testing 2D renderer.
        auto sandbox_2d_layer = std::make_unique<Sandbox2D>();
        push_layer(std::move(sandbox_2d_layer));
    }
};

std::unique_ptr<Real::Application> Real::create_application()
{
    return std::make_unique<Sandbox>();
}
