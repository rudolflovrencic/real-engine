/* ----------------------------- VERTEX SHADER ------------------------------ */
#type vertex
#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;

uniform mat4 view_projection;
uniform mat4 transform;

out vec2 varying_tex_coord;

void main()
{
    varying_tex_coord = tex_coord;
    gl_Position = view_projection
                     * transform * vec4(position, 1.0);
}
/* -------------------------------------------------------------------------- */


/* ---------------------------- FRAGMENT SHADER ----------------------------- */
#type fragment
#version 330 core

layout(location = 0) out vec4 color;

in vec2 varying_tex_coord;

uniform sampler2D texture_sampler;

void main()
{
    color = texture(texture_sampler, varying_tex_coord);
}
/* -------------------------------------------------------------------------- */
