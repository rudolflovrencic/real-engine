#include "imgui/ImGuiLayer.hpp"

#include <imgui.h>

#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

#include "core/Application.hpp"
#include <GLFW/glfw3.h> // FIXME: Do not include GLFW when context is fixed.

namespace Real {

ImGuiLayer::ImGuiLayer() : Layer("ImGuiLayer") {}

void ImGuiLayer::on_attach()
{
    // Setup ImGui context.
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable keyboard.

    ImGui::StyleColorsDark();

    ImGuiStyle& style = ImGui::GetStyle();
    ImGui::SetNextWindowBgAlpha(1.0);
    if (io.ConfigFlags) {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }
    Application& app = Application::get();
    GLFWwindow* window =
        static_cast<GLFWwindow*>(app.get_window().get_native_window());
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 410");
}

void ImGuiLayer::on_detach()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void ImGuiLayer::begin()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void ImGuiLayer::end()
{
    // Set display size.
    ImGuiIO& io      = ImGui::GetIO();
    Application& app = Application::get();
    io.DisplaySize =
        ImVec2(app.get_window().get_width(), app.get_window().get_height());

    // Render ImGui to the screen.
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    if (io.ConfigFlags) {
        GLFWwindow* backup_current_context = glfwGetCurrentContext();
        glfwMakeContextCurrent(backup_current_context);
    }
}

}
