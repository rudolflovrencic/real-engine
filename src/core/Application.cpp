#include "core/Application.hpp"

#include "core/Assert.hpp"
#include "core/Input.hpp"
#include "core/Event.hpp"
#include "renderer/Renderer.hpp"

namespace Real {

Application* Application::app_instance = nullptr;

Application::Application()
{
    // Make sure no other application instances exist (singleton).
    REAL_ASSERT(!app_instance, "Application already exists!");
    app_instance = this; // Set static pointer to constructed instance.

    // Create a window and set callback function.
    window = Window::create();
    window->set_event_callback([this](Event& event) { on_event(event); });

    Renderer::init(); // Initialize the renderer.

    // Create ImGui layer, add it to the layer stack but remember its address as
    // a member variable so it can be used later.
    auto imgui  = std::make_unique<ImGuiLayer>();
    imgui_layer = imgui.get(); // Remember non-owning pointer to ImGui layer.
    push_overlay(std::move(imgui)); // Add ImGui layer to the layer stack.
}

void Application::push_layer(std::unique_ptr<Layer> layer)
{
    layer->on_attach();
    layer_stack.push_layer(std::move(layer));
}

void Application::push_overlay(std::unique_ptr<Layer> overlay)
{
    overlay->on_attach();
    layer_stack.push_overlay(std::move(overlay));
}

const Window& Application::get_window() const noexcept
{
    return *window;
}

Application& Application::get() noexcept
{
    return *app_instance;
}

void Application::run()
{
    while (running) {
        // Calculate time taken for the loop run (timestep).
        Timepoint current_timepoint = Timestep::get_current_timepoint();
        Timestep timestep           = current_timepoint - last_frame_timepoint;
        last_frame_timepoint        = current_timepoint;

        // If window is not minimized, call on update function for each layer in
        // the layer stack. If the window is minimized, there is no need to call
        // on update for layers (this saves computing resources).
        if (!minimized) {
            for (auto& layer : layer_stack) { layer->on_update(timestep); }
        }

        // Call on ImGui render function for each layer in the layer stack. This
        // is called even if the main window has been minimized since ImGui
        // windows may still be present (otherwise, ImGui would become
        // unresponsive).
        imgui_layer->begin();
        for (auto& layer : layer_stack) { layer->on_imgui_render(); }
        imgui_layer->end();

        window->on_update();
    }
}

void Application::on_event(Event& event)
{
    // Dispatch window close event.
    std::visit(
        [this](auto&& event) {
            using T = std::decay_t<decltype(event)>;
            if constexpr (std::is_same_v<WindowCloseEvent, T>) {
                on_window_close_event(event);
            } else if constexpr (std::is_same_v<WindowResizeEvent, T>) {
                on_window_resize_event(event);
            }
        },
        event);

    // Call on event function for each layer in the layer stack.
    for (auto it = layer_stack.end(); it != layer_stack.begin();) {
        auto& layer = *(--it);
        layer->on_event(event);
        if (is_handled(event)) { break; }
    }
}

void Application::on_window_close_event(WindowCloseEvent& event)
{
    running       = false;
    event.handled = true;
}

void Application::on_window_resize_event(WindowResizeEvent& event)
{
    // If any of widnow dimensions is less or equal to minimization threshold,
    // set minimized flag to true. Minimization threshold not recommended to be
    // set to zero since some tiling window managers cannot set window
    // dimensions to zero.
    constexpr unsigned minimized_threshold = 100;
    minimized = event.get_width() <= minimized_threshold ||
                event.get_height() <= minimized_threshold;

    // Notify rendering API that the window size has changed. This is needed if
    // whole window should be used for rendering even after window has resized.
    Renderer::on_window_resize(event.get_width(), event.get_height());

    // Do not mark event as handled - all subscribed layers will be notified.
}

}
