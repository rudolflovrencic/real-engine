#include "core/Layer.hpp"

namespace Real {

Layer::Layer(std::string name) noexcept : name(std::move(name)) {}

void Layer::on_attach() {}

void Layer::on_detach() {}

void Layer::on_update(Timestep /*timestep*/) {}

void Layer::on_imgui_render() {}

void Layer::on_event(Event& /*event*/) {}

const std::string& Layer::get_name() const noexcept
{
    return name;
}

}
