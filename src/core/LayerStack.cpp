#include "core/LayerStack.hpp"

namespace Real {

void LayerStack::push_layer(std::unique_ptr<Layer> layer)
{
    layers.emplace(layers.begin() + layer_insert_index, std::move(layer));
    layer_insert_index++;
}

void LayerStack::push_overlay(std::unique_ptr<Layer> overlay)
{
    layers.emplace_back(std::move(overlay));
}

void LayerStack::pop_layer(Layer* layer)
{
    // Find layer with the provided address.
    auto it = std::find_if(layers.begin(), layers.end(), [&](std::unique_ptr<Layer>& el) {
        return el.get() == layer;
    });
    if (it != layers.end()) {
        layers.erase(it);
        layer_insert_index--;
    }
}

void LayerStack::pop_overlay(Layer* overlay)
{
    // Find overlay with the provided address.
    auto it = std::find_if(layers.begin(), layers.end(), [&](std::unique_ptr<Layer>& el) {
        return el.get() == overlay;
    });
    if (it != layers.end()) layers.erase(it);
}

}
