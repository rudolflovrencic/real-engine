#include "renderer/VertexArray.hpp"

#include "core/Assert.hpp"
#include "renderer/Renderer.hpp"
#include "platform/opengl/OpenGLVertexArray.hpp"

namespace Real {

std::shared_ptr<VertexArray> VertexArray::create()
{
    switch (Renderer::get_api()) {
        case RendererAPI::API::OpenGL:
            return std::make_shared<OpenGLVertexArray>();
        default: REAL_ASSERT_UNREACHABLE("Unsupported render API.");
    }
}

}
