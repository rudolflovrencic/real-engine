#include "renderer/RendererAPI.hpp"

namespace Real {

// Hardcoded renderer API. Could be set at runtime in the future.
RendererAPI::API RendererAPI::api = RendererAPI::API::OpenGL;

RendererAPI::API RendererAPI::get_api() noexcept
{
    return api;
}

}
