#include "renderer/Buffer.hpp"

#include "core/Assert.hpp"
#include "renderer/Renderer.hpp"
#include "platform/opengl/OpenGLBuffer.hpp"

namespace Real {

unsigned shader_data_type_size(ShaderDataType type) noexcept
{
    switch (type) {
        case ShaderDataType::Float: return 4;
        case ShaderDataType::Float2: return 4 * 2;
        case ShaderDataType::Float3: return 4 * 3;
        case ShaderDataType::Float4: return 4 * 4;
        case ShaderDataType::Mat3: return 4 * 3 * 3;
        case ShaderDataType::Mat4: return 4 * 4 * 4;
        case ShaderDataType::Int: return 4;
        case ShaderDataType::Int2: return 4 * 2;
        case ShaderDataType::Int3: return 4 * 3;
        case ShaderDataType::Int4: return 4 * 4;
        case ShaderDataType::Bool: return 1;
        case ShaderDataType::None:
            REAL_ASSERT_UNREACHABLE("'None' shader data type is not allowed!");
    }
    REAL_ASSERT_UNREACHABLE("Unhandled shader data type.");
}

BufferElement::BufferElement(ShaderDataType type,
                             std::string name,
                             bool normalized) noexcept
    : name(std::move(name)),
      type(type),
      size(shader_data_type_size(type)),
      normalized(normalized)
{}

unsigned BufferElement::get_component_count() const noexcept
{
    switch (type) {
        case ShaderDataType::Float: return 1;
        case ShaderDataType::Float2: return 2;
        case ShaderDataType::Float3: return 3;
        case ShaderDataType::Float4: return 4;
        case ShaderDataType::Mat3: return 3 * 3;
        case ShaderDataType::Mat4: return 4 * 4;
        case ShaderDataType::Int: return 1;
        case ShaderDataType::Int2: return 2;
        case ShaderDataType::Int3: return 3;
        case ShaderDataType::Int4: return 4;
        case ShaderDataType::Bool: return 1;
        case ShaderDataType::None:
            REAL_ASSERT_UNREACHABLE("'None' shader data type is not allowed!");
    }
    REAL_ASSERT_UNREACHABLE("Unhandled shader data type.");
}

BufferLayout::BufferLayout(std::initializer_list<BufferElement> elements)
    : elements(std::move(elements))
{
    calculate_offsets_and_stride();
}

const std::vector<BufferElement>& BufferLayout::get_elements() const noexcept
{
    return elements;
}

unsigned BufferLayout::get_stride() const noexcept
{
    return stride;
}

std::vector<BufferElement>::iterator BufferLayout::begin() noexcept
{
    return elements.begin();
}

std::vector<BufferElement>::iterator BufferLayout::end() noexcept
{
    return elements.end();
}

std::vector<BufferElement>::const_iterator BufferLayout::begin() const noexcept
{
    return elements.begin();
}

std::vector<BufferElement>::const_iterator BufferLayout::end() const noexcept
{
    return elements.end();
}

void BufferLayout::calculate_offsets_and_stride() noexcept
{
    unsigned offset = 0;
    stride          = 0;
    for (auto& element : elements) {
        element.offset = offset;
        offset += element.size;
        stride += element.size;
    }
}

std::shared_ptr<VertexBuffer> VertexBuffer::create(float* vertices,
                                                   unsigned count)
{
    switch (Renderer::get_api()) {
        case RendererAPI::API::OpenGL:
            return std::make_shared<OpenGLVertexBuffer>(vertices, count);
        default: REAL_ASSERT_UNREACHABLE("Unhandled render API.");
    }
}

std::shared_ptr<IndexBuffer> IndexBuffer::create(unsigned* indices,
                                                 unsigned count)
{
    switch (Renderer::get_api()) {
        case RendererAPI::API::OpenGL:
            return std::make_shared<OpenGLIndexBuffer>(indices, count);
        default: REAL_ASSERT_UNREACHABLE("Unhandled render API.");
    }
}

}
