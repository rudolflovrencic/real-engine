#include "platform/linux/LinuxWindow.hpp"
#include "platform/opengl/OpenGLContext.hpp"

#include <GLFW/glfw3.h>

#include "core/Assert.hpp"
#include "core/Core.hpp"
#include "core/Event.hpp"

namespace Real {

namespace {
bool glfw_initialized                   = false;
constexpr std::array ignore_error_codes = {65544};
void glfw_error_callback(int error, const char* description) noexcept;
}

LinuxWindow::LinuxWindow(const WindowProperties& props)
{
    initialize(props);
}

LinuxWindow::~LinuxWindow()
{
    shutdown();
}

LinuxWindow::LinuxWindow(LinuxWindow&& other) noexcept
    : window(std::move(other.window)),
      data(std::move(other.data)),
      input(std::move(other.input)),
      context(std::move(other.context))
{
    other.window = nullptr;
}

LinuxWindow& LinuxWindow::operator=(LinuxWindow&& other) noexcept
{
    shutdown(); // Clean up this window.

    window  = std::move(other.window);
    data    = std::move(other.data);
    input   = std::move(other.input);
    context = std::move(other.context);

    other.window = nullptr; // Remove ownership of GLFW window from other.

    return *this;
}

void LinuxWindow::on_update()
{
    glfwPollEvents();
    context->swap_buffers();
}

unsigned LinuxWindow::get_width() const
{
    return data.width;
}

unsigned LinuxWindow::get_height() const
{
    return data.height;
}

void LinuxWindow::set_event_callback(EventCallback event_callback)
{
    data.event_callback = std::move(event_callback);
}

void LinuxWindow::set_vsync(bool enabled)
{
    if (enabled) {
        glfwSwapInterval(1);
    } else {
        glfwSwapInterval(0);
    }
    data.vsync = enabled;
}

bool LinuxWindow::is_vsync() const
{
    return data.vsync;
}

void* LinuxWindow::get_native_window() const
{
    return window;
}

void LinuxWindow::initialize(const WindowProperties& props)
{
    data.title  = props.title;
    data.width  = props.width;
    data.height = props.height;

    Log::Core::success(
        "Creating window {0} ({1}, {2})", data.title, data.width, data.height);

    if (!glfw_initialized) {
        [[maybe_unused]] auto status = glfwInit();
        REAL_ASSERT(status, "Failed to initialize GLFW!");
        glfwSetErrorCallback(glfw_error_callback);
        glfw_initialized = true;
    }

    window = glfwCreateWindow(static_cast<int>(data.width),
                              static_cast<int>(data.height),
                              data.title.c_str(),
                              nullptr,
                              nullptr);

    // Create a rendering context.
    context = std::make_unique<OpenGLContext>(window);
    context->initialize();

    glfwSetWindowUserPointer(window, &data);
    set_vsync(true);

    set_glfw_callbacks(); // Set window callback functions.
}

void LinuxWindow::shutdown() noexcept
{
    if (window) { glfwDestroyWindow(window); }
}

void LinuxWindow::set_glfw_callbacks() noexcept
{
    // Window resize callback function.
    glfwSetWindowSizeCallback(
        window, [](GLFWwindow* win, int width, int height) {
            WindowData& data =
                *static_cast<WindowData*>(glfwGetWindowUserPointer(win));
            data.width  = width;
            data.height = height;

            Log::Core::normal("Window size: ({}, {})", width, height);
            Event event{WindowResizeEvent(width, height)};
            data.event_callback(event);
        });

    // Window close callback function.
    glfwSetWindowCloseCallback(window, [](GLFWwindow* win) {
        WindowData& data =
            *static_cast<WindowData*>(glfwGetWindowUserPointer(win));
        Event event{WindowCloseEvent()};
        data.event_callback(event);
    });

    // Key callback function.
    glfwSetKeyCallback(
        window, [](GLFWwindow* win, int key, int, int action, int) {
            WindowData& data =
                *static_cast<WindowData*>(glfwGetWindowUserPointer(win));
            switch (action) {
                case GLFW_PRESS: {
                    Event event{KeyPressedEvent(key, 0)};
                    data.event_callback(event);
                    break;
                }
                case GLFW_RELEASE: {
                    Event event{KeyReleasedEvent(key)};
                    data.event_callback(event);
                    break;
                }
                case GLFW_REPEAT: {
                    // Number of repeats is currently always set to one. In
                    // the future, it might be worth counting number of key
                    // repeats.
                    Event event{KeyPressedEvent(key, 1)};
                    data.event_callback(event);
                    break;
                }
            }
        });

    // Key typed (character) callback function.
    glfwSetCharCallback(window, [](GLFWwindow* win, unsigned keycode) {
        WindowData& data =
            *static_cast<WindowData*>(glfwGetWindowUserPointer(win));
        Event event{KeyTypedEvent(keycode)};
        data.event_callback(event);
    });

    // Mouse button callback function.
    glfwSetMouseButtonCallback(
        window, [](GLFWwindow* win, int button, int action, int) {
            WindowData& data =
                *static_cast<WindowData*>(glfwGetWindowUserPointer(win));
            switch (action) {
                case GLFW_PRESS: {
                    Event event{MouseButtonPressedEvent(button)};
                    data.event_callback(event);
                    break;
                }
                case GLFW_RELEASE: {
                    Event event{MouseButtonReleasedEvent(button)};
                    data.event_callback(event);
                    break;
                }
            }
        });

    // Mouse scroll callback function.
    glfwSetScrollCallback(
        window, [](GLFWwindow* win, double x_offset, double y_offset) {
            WindowData& data =
                *static_cast<WindowData*>(glfwGetWindowUserPointer(win));
            Event event{MouseScrolledEvent(static_cast<float>(x_offset),
                                           static_cast<float>(y_offset))};
            data.event_callback(event);
        });

    // Mouse cursor position callback function.
    glfwSetCursorPosCallback(
        window, [](GLFWwindow* win, double x_position, double y_position) {
            WindowData& data =
                *static_cast<WindowData*>(glfwGetWindowUserPointer(win));

            Event event{MouseMovedEvent(static_cast<float>(x_position),
                                        static_cast<float>(y_position))};
            data.event_callback(event);
        });
}

namespace {
void glfw_error_callback(int error, const char* description) noexcept
{
    if (std::find(ignore_error_codes.begin(),
                  ignore_error_codes.end(),
                  error) == ignore_error_codes.end()) {
        Log::Core::error("GLFW error ({}): {}", error, description);
        std::abort();
    }
}
}

}
