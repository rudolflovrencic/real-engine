#include "platform/opengl/OpenGLRendererAPI.hpp"

#include <GL/glew.h>

namespace Real {

namespace {

[[maybe_unused]] void enable_opengl_debug() noexcept;

[[maybe_unused]] void
opengl_error_callback(GLenum source,
                      GLenum type,
                      GLuint id,
                      GLenum severity,
                      GLsizei length,
                      const GLchar* message,
                      const void* user_parameters) noexcept;

}

void OpenGLRendererAPI::init() noexcept
{
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);

#ifndef REAL_NGLDEBUG
    enable_opengl_debug();
#endif
}

void OpenGLRendererAPI::set_viewport(unsigned x,
                                     unsigned y,
                                     unsigned width,
                                     unsigned height) noexcept
{
    glViewport(x, y, width, height);
}

void OpenGLRendererAPI::set_clear_color(const glm::vec4& color) noexcept
{
    glClearColor(color.r, color.g, color.b, color.a);
}

void OpenGLRendererAPI::clear() noexcept
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGLRendererAPI::draw_indexed(const VertexArray& vertex_array) noexcept
{
    glDrawElements(GL_TRIANGLES,
                   vertex_array.get_index_buffer()->get_count(),
                   GL_UNSIGNED_INT,
                   nullptr);
}

namespace {

void enable_opengl_debug() noexcept
{
    // Ensure debug messages are enabled.
    glEnable(GL_DEBUG_OUTPUT);

    // Ensure callback is called by same thread so stack-trace is meaningful.
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

    // Register a callback function.
    glDebugMessageCallback(opengl_error_callback, nullptr);

    // Ensure all possible callback messages are received.
    glDebugMessageControl(
        GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, true);
}

void opengl_error_callback(GLenum source,
                           GLenum type,
                           GLuint id,
                           GLenum severity,
                           GLsizei /*length*/,
                           const GLchar* message,
                           const void* /*user_parameters*/) noexcept
{
    // String representations of severity, source and type codes.
    std::string_view sev, src, typ;

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH: sev = "HIGH"; break;
        case GL_DEBUG_SEVERITY_MEDIUM: sev = "MEDIUM"; break;
        case GL_DEBUG_SEVERITY_LOW: sev = "LOW"; break;
        default: return; // Only report selected severities.
    }

    switch (source) {
        case GL_DEBUG_SOURCE_API: src = "API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: src = "WINDOW SYSTEM"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: src = "SHADER COMPILER"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY: src = "THIRD PARTY"; break;
        case GL_DEBUG_SOURCE_APPLICATION: src = "APPLICATION"; break;
        case GL_DEBUG_SOURCE_OTHER: src = "UNKNOWN"; break;
        default: src = "UNKNOWN"; break;
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR: typ = "ERROR"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            typ = "DEPRECATED BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            typ = "UNDEFINED BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_PORTABILITY: typ = "PORTABILITY"; break;
        case GL_DEBUG_TYPE_PERFORMANCE: typ = "PERFORMANCE"; break;
        case GL_DEBUG_TYPE_OTHER: typ = "OTHER"; break;
        case GL_DEBUG_TYPE_MARKER: typ = "MARKER"; break;
        default: typ = "UNKNOWN"; break;
    }

    Real::Log::Core::error("OpenGL Error {} of type {} and {} severity raised "
                           "from {}. Message: {}",
                           id,
                           typ,
                           sev,
                           src,
                           message);
    std::abort();
}

}

}
