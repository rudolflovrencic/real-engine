#include "platform/opengl/OpenGLTexture.hpp"

#include "core/Assert.hpp"

#include <GL/glew.h>
#include <stb_image.h>

namespace Real {

OpenGLTexture2D::OpenGLTexture2D(std::filesystem::path filepath)
    : filepath(std::move(filepath))
{
    // Read image from bottom to top since OpenGL expects it like that.
    stbi_set_flip_vertically_on_load(1);

    // Load the image and do basic validation.
    int w, h, c; // Width, height and number of channels.
    unsigned char* image_data =
        stbi_load(this->filepath.c_str(), &w, &h, &c, 0);
    REAL_ASSERT_FMT(image_data, "Failed to load texture {}!", this->filepath);

    GLenum internal_format = c == 4 ? GL_RGBA8 : c == 3 ? GL_RGB8 : 0;
    GLenum data_format     = c == 4 ? GL_RGBA : c == 3 ? GL_RGB : 0;
    REAL_ASSERT(internal_format && data_format, "Unsupported image format.");

    // Set properties from loaded data (STB sets signed values, so this
    // assignment is needed for conversion since OpenGL expect unsigned values).
    width  = w;
    height = h;

    // Create a storage for the texture in GPU memory.
    glCreateTextures(GL_TEXTURE_2D, 1, &renderer_id);
    glTextureStorage2D(renderer_id, 1, internal_format, width, height);

    // Set shrinking and magnification filtering.
    glTextureParameteri(renderer_id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(renderer_id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // Set texutre wrapping mode.
    glTextureParameteri(renderer_id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTextureParameteri(renderer_id, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Upload texture data to the GPU memory.
    glTextureSubImage2D(renderer_id,
                        0,
                        0,
                        0,
                        width,
                        height,
                        data_format,
                        GL_UNSIGNED_BYTE,
                        image_data);

    // Free allocated memory by the STB library.
    stbi_image_free(image_data);
}

OpenGLTexture2D::~OpenGLTexture2D()
{
    glDeleteTextures(1, &renderer_id);
}

void OpenGLTexture2D::bind(unsigned slot) const
{
    glBindTextureUnit(slot, renderer_id);
}

}
