#include "platform/opengl/OpenGLShader.hpp"

#include "core/Assert.hpp"

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

#include <fstream>

namespace Real {

enum class ShaderType : unsigned {
    Vertex   = GL_VERTEX_SHADER,
    Fragment = GL_FRAGMENT_SHADER
};

namespace {
ShaderType shader_type_from_string(const std::string& type)
{
    if (type == "vertex") { return ShaderType::Vertex; }
    if (type == "fragment" || type == "pixel") { return ShaderType::Fragment; }
    REAL_ASSERT_UNREACHABLE("Unknown shader type!");
}
}

/** Encapsulation of a single shader program that has to be compiled and linked
 * with other single shader in order to form a shader program runnable on a
 * system GPU. */
class SingleShader {
  private:
    unsigned renderer_id;
    std::string source_code;
    ShaderType type;

  public:
    SingleShader(ShaderType type, const std::string& shader_src);
    ~SingleShader();

    void compile();
    void attach(unsigned program_id) const;
    void detach(unsigned program_id) const;
};

SingleShader::SingleShader(ShaderType type,
                           const std::string& shader_source_code)
    : renderer_id(0), source_code(shader_source_code), type(type)
{}

SingleShader::~SingleShader()
{
    glDeleteShader(renderer_id);
}

void SingleShader::attach(unsigned program_id) const
{
    glAttachShader(program_id, renderer_id);
}

void SingleShader::detach(unsigned program_id) const
{
    glDetachShader(program_id, renderer_id);
}

void SingleShader::compile()
{
    REAL_ASSERT(renderer_id == 0, "Shader compiled multiple times!");
    renderer_id    = glCreateShader(static_cast<GLenum>(type));
    char* code_ptr = const_cast<char*>(source_code.c_str());
    glShaderSource(renderer_id, 1, &code_ptr, nullptr);
    glCompileShader(renderer_id);

    // Error handling.
    int result;
    glGetShaderiv(renderer_id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) { // Shader did not compile properly.
        int message_length;   // Error message length.
        glGetShaderiv(renderer_id, GL_INFO_LOG_LENGTH, &message_length);
        std::string msg(message_length + 1, '\0'); // Reserve message space.
        glGetShaderInfoLog(renderer_id,
                           message_length,
                           &message_length,
                           const_cast<char*>(msg.c_str()));
        glDeleteShader(renderer_id);
        REAL_ASSERT_FMT(false,
                        "Failed to compile {0} shader:\n{1}",
                        type == ShaderType::Vertex ?
                            "vertex" :
                            type == ShaderType::Fragment ? "fragment" :
                                                           "unknown type",
                        msg);
    }
}

OpenGLShader::OpenGLShader(const std::filesystem::path& filepath)
    : name(filepath.stem())
{
    // Read a file and create a mapping of shader type and shader source.
    std::string shader_source = read_file(filepath);
    auto shader_sources       = preprocess(shader_source);

    // Create all shaders from provided sources.
    std::vector<SingleShader> shaders;
    shaders.reserve(shader_sources.size());
    for (auto& [shader_type, shader_src] : shader_sources) {
        shaders.emplace_back(shader_type, shader_src);
    }

    // Compile all shaders.
    for (auto& shader : shaders) shader.compile();

    // Get a shader program object handle and attach all shaders to it.
    renderer_id = glCreateProgram();
    for (auto& shader : shaders) shader.attach(renderer_id);

    // Link and validate complete shader program.
    link_shader();

    // Detach used shaders after compilation and successful linkage.
    for (auto& shader : shaders) shader.detach(renderer_id);
}

OpenGLShader::OpenGLShader(const std::string& name,
                           const std::string& vertex_src,
                           const std::string& fragment_src)
    : name(name)
{
    // Create and compile a vertex shader.
    SingleShader vertex(ShaderType::Vertex, vertex_src);
    vertex.compile();

    // Create and compile a fragment shader.
    SingleShader fragment(ShaderType::Fragment, fragment_src);
    fragment.compile();

    // Get a shader program object handle and attach both shaders to it.
    renderer_id = glCreateProgram();
    vertex.attach(renderer_id);
    fragment.attach(renderer_id);

    // Link and validate complete shader program.
    link_shader();

    // Detach used shaders after compilation and successful linkage.
    vertex.detach(renderer_id);
    fragment.detach(renderer_id);
}

OpenGLShader::~OpenGLShader()
{
    glDeleteProgram(renderer_id);
}

void OpenGLShader::bind() const
{
    glUseProgram(renderer_id);
}

void OpenGLShader::unbind() const
{
    glUseProgram(0);
}

void OpenGLShader::set_uniform_int(const std::string& name, int value)
{
    upload_uniform_int(name, value);
}

void OpenGLShader::set_uniform_float3(const std::string& name,
                                      const glm::vec3& vec)
{
    upload_uniform_float3(name, vec);
}

void OpenGLShader::set_uniform_float4(const std::string& name,
                                      const glm::vec4& vec)
{
    upload_uniform_float4(name, vec);
}

void OpenGLShader::set_uniform_mat4(const std::string& name,
                                    const glm::mat4& mat)
{
    upload_uniform_mat4(name, mat);
}

void OpenGLShader::upload_uniform_int(const std::string& name, int value)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniform1i(location, value);
}

void OpenGLShader::upload_uniform_float(const std::string& name, float value)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniform1f(location, value);
}

void OpenGLShader::upload_uniform_float2(const std::string& name,
                                         const glm::vec2& vec)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniform2f(location, vec.x, vec.y);
}

void OpenGLShader::upload_uniform_float3(const std::string& name,
                                         const glm::vec3& vec)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniform3f(location, vec.x, vec.y, vec.z);
}

void OpenGLShader::upload_uniform_float4(const std::string& name,
                                         const glm::vec4& vec)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniform4f(location, vec.x, vec.y, vec.z, vec.w);
}

void OpenGLShader::upload_uniform_mat3(const std::string& name,
                                       const glm::mat3& matrix)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void OpenGLShader::upload_uniform_mat4(const std::string& name,
                                       const glm::mat4& matrix)
{
    GLint location = glGetUniformLocation(renderer_id, name.c_str());
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

std::string OpenGLShader::read_file(const std::filesystem::path& filepath)
{
    std::string file_contents;
    std::ifstream infs(filepath, std::ios::in | std::ios::binary);
    if (infs) {
        infs.seekg(0, std::ios::end);
        file_contents.resize(infs.tellg());
        infs.seekg(0, std::ios::beg);
        infs.read(&file_contents[0], file_contents.size());
    } else {
        Log::Core::error("Failed to open a shader file {}", filepath);
    }
    return file_contents;
}

std::unordered_map<ShaderType, std::string>
OpenGLShader::preprocess(const std::string& src)
{
    std::unordered_map<ShaderType, std::string> sources;

    const char* type_token  = "#type";
    size_t type_token_ength = std::strlen(type_token);
    size_t pos              = src.find(type_token, 0);
    while (pos != std::string::npos) {
        size_t eol = src.find_first_of("\r\n", pos);
        REAL_ASSERT(eol != std::string::npos, "Syntax error in shader.");
        size_t begin     = pos + type_token_ength + 1;
        std::string type = src.substr(begin, eol - begin);

        // Trim type string from the left and right.
        type.erase(type.begin(),
                   std::find_if(type.begin(), type.end(), [](int ch) {
                       return !std::isspace(ch);
                   }));
        type.erase(std::find_if(type.rbegin(),
                                type.rend(),
                                [](int ch) { return !std::isspace(ch); })
                       .base(),
                   type.end());

        size_t next_line_pos = src.find_first_not_of("\r\n", eol);
        pos                  = src.find(type_token, next_line_pos);

        sources[shader_type_from_string(type)] = src.substr(
            next_line_pos,
            pos - (next_line_pos == std::string::npos ? src.size() - 1 :
                                                        next_line_pos));
    }

    return sources;
}

void OpenGLShader::link_shader() const
{
    // Link and validate complete shader program.
    glLinkProgram(renderer_id);
    glValidateProgram(renderer_id);

    // Make sure program has linked correctly.
    GLint is_linked = 0;
    glGetProgramiv(renderer_id, GL_LINK_STATUS, &is_linked);
    if (is_linked == GL_FALSE) {
        // Get the linking log length.
        GLint log_length = 0;
        glGetShaderiv(renderer_id, GL_INFO_LOG_LENGTH, &log_length);

        // Get the linking log.
        std::vector<GLchar> info_log(log_length);
        glGetProgramInfoLog(renderer_id, log_length, &log_length, &info_log[0]);

        // Delete failed program handle and both compiled shaders.
        glDeleteProgram(renderer_id);

        REAL_ASSERT_UNREACHABLE_FMT("Shader linking failed:\n{0}",
                                    info_log.data());
    }
}

}
