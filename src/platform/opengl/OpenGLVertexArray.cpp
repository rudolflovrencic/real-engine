#include "platform/opengl/OpenGLVertexArray.hpp"

#include "core/Assert.hpp"
#include <GL/glew.h>

namespace Real {

namespace {
GLenum shader_data_type_to_opengl_base_type(Real::ShaderDataType type)
{
    switch (type) {
        case Real::ShaderDataType::Float: return GL_FLOAT;
        case Real::ShaderDataType::Float2: return GL_FLOAT;
        case Real::ShaderDataType::Float3: return GL_FLOAT;
        case Real::ShaderDataType::Float4: return GL_FLOAT;
        case Real::ShaderDataType::Mat3: return GL_FLOAT;
        case Real::ShaderDataType::Mat4: return GL_FLOAT;
        case Real::ShaderDataType::Int: return GL_INT;
        case Real::ShaderDataType::Int2: return GL_INT;
        case Real::ShaderDataType::Int3: return GL_INT;
        case Real::ShaderDataType::Int4: return GL_INT;
        case Real::ShaderDataType::Bool: return GL_BOOL;
        case Real::ShaderDataType::None:
            REAL_ASSERT_UNREACHABLE("'None' shader data type is not allowed!");
    }
    REAL_ASSERT_UNREACHABLE("Unhandled shader data type.");
}
}

OpenGLVertexArray::OpenGLVertexArray() noexcept
{
    glCreateVertexArrays(1, &renderer_id);
}

OpenGLVertexArray::~OpenGLVertexArray() noexcept
{
    if (renderer_id) { glDeleteVertexArrays(1, &renderer_id); }
}

OpenGLVertexArray::OpenGLVertexArray(OpenGLVertexArray&& other) noexcept
    : renderer_id(other.renderer_id),
      vertex_buffers(std::move(other.vertex_buffers)),
      index_buffer(std::move(other.index_buffer))
{
    other.renderer_id = 0;
}

OpenGLVertexArray&
OpenGLVertexArray::operator=(OpenGLVertexArray&& other) noexcept
{
    // Clean up this object.
    if (renderer_id != 0) { glDeleteVertexArrays(1, &renderer_id); }

    renderer_id    = other.renderer_id;
    vertex_buffers = std::move(other.vertex_buffers);
    index_buffer   = std::move(other.index_buffer);

    other.renderer_id = 0;

    return *this;
}

void OpenGLVertexArray::bind() const noexcept
{
    glBindVertexArray(renderer_id);
}

void OpenGLVertexArray::unbind() const noexcept
{
    glBindVertexArray(0);
}

void OpenGLVertexArray::add_vertex_buffer(
    std::shared_ptr<VertexBuffer> vertex_buffer)
{
    // Make sure layout has been set for the vertex buffer since layout is
    // remembered inside vertex array object.
    REAL_ASSERT(vertex_buffer->get_layout().get_elements().size(),
                "Provided vertex buffer has no layout.");

    glBindVertexArray(renderer_id);
    vertex_buffer->bind();

    unsigned index     = 0;
    const auto& layout = vertex_buffer->get_layout();
    for (const auto& element : layout) {
        glEnableVertexAttribArray(index);
        glVertexAttribPointer(
            index, // Index of the vertex buffer element.
            element.get_component_count(), // Count of components.
            shader_data_type_to_opengl_base_type(element.type), // Type.
            element.normalized ? GL_TRUE : GL_FALSE, // Normalized flag.
            layout.get_stride(), // Size of each vertex (stride).
            reinterpret_cast<const void*>(element.offset)); // Offset.
        index++;
    }

    vertex_buffers.push_back(std::move(vertex_buffer));
}

void OpenGLVertexArray::set_index_buffer(
    std::shared_ptr<IndexBuffer> index_buffer) noexcept
{
    // Bind vertex array first and then bind the index buffer so the index
    // buffer becomes associated with that vertex array.
    glBindVertexArray(renderer_id);
    index_buffer->bind();

    this->index_buffer = std::move(index_buffer);
}

const std::shared_ptr<IndexBuffer>&
OpenGLVertexArray::get_index_buffer() const noexcept
{
    return index_buffer;
}

const std::vector<std::shared_ptr<VertexBuffer>>&
OpenGLVertexArray::get_vertex_buffers() const noexcept
{
    return vertex_buffers;
}

}
