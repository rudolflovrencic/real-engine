#include "platform/opengl/OpenGLContext.hpp"

#include "core/Assert.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace Real {

OpenGLContext::OpenGLContext(GLFWwindow* window) : window(window)
{
    REAL_ASSERT(window, "Window handle passed to OpenGL context is null!");
}

void OpenGLContext::initialize()
{
    glfwMakeContextCurrent(window);
    [[maybe_unused]] bool glew_init_successful = glewInit() == GLEW_OK;
    REAL_ASSERT(glew_init_successful, "Failed to initialize GLEW.");
}

void OpenGLContext::swap_buffers()
{
    glfwSwapBuffers(window);
}

}
