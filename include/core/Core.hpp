#ifndef REAL_CORE_HPP
#define REAL_CORE_HPP

#include <limits>
#include <stdexcept>
#include <type_traits>

/* Platform check. */
#if not defined __linux__
    #error "Real engine currently supports only Linux."
#endif

namespace Real {

/** Function for defining bitfields. Bit that fit into standard unsigned type
 * are supported. */
constexpr unsigned bitfield(unsigned char x)
{
    if (x >= std::numeric_limits<unsigned>::digits) {
        throw std::logic_error("Bitfield out of range.");
    }
    return 1U << x;
}

template<typename /*T*/>
constexpr bool AlwaysFalse = false;

template<typename T, typename... U>
constexpr bool is_any_of_v = (std::is_same_v<T, U> || ...);

template<typename Base, typename Derived>
constexpr bool is_public_base = std::is_convertible_v<Derived*, Base*>;

}

#endif
