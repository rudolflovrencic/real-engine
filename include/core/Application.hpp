#ifndef REAL_APPLICATION_HPP
#define REAL_APPLICATION_HPP

#include <memory>

#include "Core.hpp"
#include "Window.hpp"
#include "Layer.hpp"
#include "LayerStack.hpp"
#include "core/Timestep.hpp"
#include "core/Event.hpp"
#include "imgui/ImGuiLayer.hpp"

namespace Real {

/** Real engine application abstraction. Only one instance of application object
 * is allowed so it is a singleton. */
class Application {
  private:
    std::unique_ptr<Window> window;
    bool running   = true;
    bool minimized = false;
    LayerStack layer_stack;
    ImGuiLayer* imgui_layer; ///< Pointer to ImGui layer at the layer stack.
    Timepoint last_frame_timepoint; ///< Timepoint when previous frame finished.

    static Application* app_instance;

  public:
    Application();
    virtual ~Application()                = default;
    Application(const Application& other) = delete;
    Application(Application&& other)      = default;
    Application& operator=(const Application& other) = delete;
    Application& operator=(Application&& other) = default;

    void run();
    void on_event(Event& event);
    void push_layer(std::unique_ptr<Layer> layer);
    void push_overlay(std::unique_ptr<Layer> overlay);

    const Window& get_window() const noexcept;

    /** Returns a reference to the instance of the real application. Only one
     * application is allowed since this class is a singleton. This function
     * enables fetching of application instnace anywhere in the codebase. */
    static Application& get() noexcept;

  private:
    void on_window_close_event(WindowCloseEvent& event);
    void on_window_resize_event(WindowResizeEvent& event);
};

/** Creates a Real engine appliaction and returns a unique pointer to it. Client
 * application has to implement this function. */
std::unique_ptr<Application> create_application();

}

#endif
