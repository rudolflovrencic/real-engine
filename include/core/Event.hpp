#ifndef REAL_EVENT_HPP
#define REAL_EVENT_HPP

#include "core/Core.hpp"

#include <string>
#include <functional>
#include <string_view>
#include <variant>

namespace Real {

/* All events in are currently blocking, meaning when an event occurs it
 * immediately gets dispatched and must be dealt with right then and there. For
 * the future, a better strategy might be to buffer events in an event bus and
 * process then during the "event" part of the update stage. */

class EventBase {
  public:
    enum class Category : unsigned {
        Application = bitfield(0),
        Input       = bitfield(1),
        Keyboard    = bitfield(2),
        Mouse       = bitfield(3),
        MouseButton = bitfield(4)
    };

  public:
    bool handled = false;

  private:
    Category category;

  public:
    explicit EventBase(Category category) noexcept;
    virtual ~EventBase() = default;

    bool is_in_category(Category category) const noexcept;

    virtual std::string_view get_name() const noexcept = 0;
    virtual std::string get_information() const noexcept;
};

EventBase::Category operator|(EventBase::Category first,
                              EventBase::Category second) noexcept;

class KeyEvent : public EventBase {
  private:
    int keycode;

  public:
    int get_keycode() const noexcept;

  protected:
    explicit KeyEvent(int keycode) noexcept;
};

class KeyPressedEvent : public KeyEvent {
  private:
    unsigned repeat_count;

  public:
    KeyPressedEvent(int keycode, int repeat_count) noexcept;
    unsigned get_repeat_count() const noexcept;
    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class KeyReleasedEvent : public KeyEvent {
  public:
    explicit KeyReleasedEvent(int keycode) noexcept;
    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class KeyTypedEvent : public KeyEvent {
  public:
    explicit KeyTypedEvent(int keycode) noexcept;
    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class MouseMovedEvent : public EventBase {
  private:
    double x; // Mouse X offset (horizontal scrolling).
    double y; // Mouse Y offset (vertical scrolling).

  public:
    MouseMovedEvent(double x, double y) noexcept;

    double get_x() const noexcept;
    double get_y() const noexcept;

    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class MouseScrolledEvent : public EventBase {
  private:
    double x_offset; // Mouse X offset (horizontal scrolling).
    double y_offset; // Mouse Y offset (vertical scrolling).

  public:
    MouseScrolledEvent(double x_offset, double y_offset) noexcept;

    double get_x_offset() const noexcept;
    double get_y_offset() const noexcept;

    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class MouseButtonEvent : public EventBase {
  private:
    int button; // Mouse button code.

  public:
    int get_button() const noexcept;

  protected:
    explicit MouseButtonEvent(int button) noexcept;
};

class MouseButtonPressedEvent : public MouseButtonEvent {
  public:
    explicit MouseButtonPressedEvent(int button) noexcept;
    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class MouseButtonReleasedEvent : public MouseButtonEvent {
  public:
    explicit MouseButtonReleasedEvent(int button) noexcept;
    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class WindowResizeEvent : public EventBase {
  private:
    unsigned width;
    unsigned height;

  public:
    WindowResizeEvent(unsigned width, unsigned height) noexcept;

    unsigned get_width() const noexcept;
    unsigned get_height() const noexcept;

    std::string_view get_name() const noexcept override;
    std::string get_information() const noexcept override;
};

class WindowCloseEvent : public EventBase {
  public:
    WindowCloseEvent() noexcept;
    std::string_view get_name() const noexcept override;
};

class AppTickEvent : public EventBase {
  public:
    AppTickEvent() noexcept;
    std::string_view get_name() const noexcept override;
};

class AppUpdateEvent : public EventBase {
  public:
    AppUpdateEvent() noexcept;
    std::string_view get_name() const noexcept override;
};

class AppRenderEvent : public EventBase {
  public:
    AppRenderEvent() noexcept;
    std::string_view get_name() const noexcept override;
};

using Event = std::variant<KeyPressedEvent,
                           KeyReleasedEvent,
                           KeyTypedEvent,
                           MouseMovedEvent,
                           MouseScrolledEvent,
                           MouseButtonPressedEvent,
                           MouseButtonReleasedEvent,
                           WindowResizeEvent,
                           WindowCloseEvent,
                           AppTickEvent,
                           AppUpdateEvent,
                           AppRenderEvent>;

bool is_handled(const Event& event) noexcept;

}

#endif
