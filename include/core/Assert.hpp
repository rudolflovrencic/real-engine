#ifndef REAL_ASSERT_HPP
#define REAL_ASSERT_HPP

#include "core/Log.hpp"
#include <cstdlib>

#ifndef NDEBUG
    #define REAL_ASSERT(expression, message)                        \
        do {                                                        \
            if (!(expression)) {                                    \
                ::Real::Log::error("[ASSERTION FAILED] ({}:{}) {}", \
                                   __FILE__,                        \
                                   __LINE__,                        \
                                   message);                        \
                ::std::abort();                                     \
            }                                                       \
        } while (false)

    #define REAL_ASSERT_FMT(expression, format_string, ...)               \
        do {                                                              \
            if (!(expression)) {                                          \
                auto message = ::fmt::format(format_string, __VA_ARGS__); \
                ::Real::Log::error("[ASSERTION FAILED] ({}:{}) {}",       \
                                   __FILE__,                              \
                                   __LINE__,                              \
                                   message);                              \
                ::std::abort();                                           \
            }                                                             \
        } while (false)

    #define REAL_ASSERT_UNREACHABLE(message)                                 \
        do {                                                                 \
            ::Real::Log::error("[UNREACHABLE STATEMENT REACHED] ({}:{}) {}", \
                               __FILE__,                                     \
                               __LINE__,                                     \
                               message);                                     \
            ::std::abort();                                                  \
        } while (false)

    #define REAL_ASSERT_UNREACHABLE_FMT(format_string, ...)                    \
        do {                                                                   \
            auto message = ::fmt::format(format_string, __VA_ARGS__);          \
            ::Real::Log::error(                                                \
                "[ASSERTION FAILED] ({}:{}) {}", __FILE__, __LINE__, message); \
            ::std::abort();                                                    \
        } while (false)

#else
    #define REAL_ASSERT(expression, message)
    #define REAL_ASSERT_FMT(expression, format_string, ...)
    #define REAL_ASSERT_UNREACHABLE(message)          ::std::abort()
    #define REAL_ASSERT_UNREACHABLE_FMT(message, ...) ::std::abort()
#endif

#endif
