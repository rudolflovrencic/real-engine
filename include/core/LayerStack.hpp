#ifndef REAL_LAYER_STACK_HPP
#define REAL_LAYER_STACK_HPP

#include "Core.hpp"
#include "Layer.hpp"

#include <vector>
#include <memory>

namespace Real {

/** Layer stack abstraction that consists of layers and overlays. Overlays are
 * always rendered on top of layers and hence are contained at the end of the
 * stack. Internally, layer stack is implemented as a vector since overlays are
 * inserted at the end. Fast iteration is also required since layer stack is
 * iterated each frame. Layers pushed into the stack are owned by the stack. */
class LayerStack {
    using LayerStackCollection = std::vector<std::unique_ptr<Layer>>;

  private:
    LayerStackCollection layers;
    unsigned layer_insert_index = 0;

  public:
    /** Pushes layer into the layer stack. Layer stack owns pushed layer. */
    void push_layer(std::unique_ptr<Layer> layer);

    /** Pushes overlay into the layer stack. Layer stack owns pushed overlay. */
    void push_overlay(std::unique_ptr<Layer> overlay);

    /** Removes layer from the layer stack (deletes the layer object). */
    void pop_layer(Layer* layer);

    /** Removes overlay from the layer stack (deletes the layer object). */
    void pop_overlay(Layer* overlay);

    LayerStackCollection::iterator begin() { return layers.begin(); }
    LayerStackCollection::iterator end() { return layers.end(); }
};

}

#endif
