#ifndef REAL_LAYER_HPP
#define REAL_LAYER_HPP

#include "Core.hpp"
#include "Event.hpp"
#include "core/Timestep.hpp"

namespace Real {

class Layer {
  private:
    /** Name of the layer. This should only be used in debug builds to easily
     * identify layers. Never use layer name for find a layer you are looking
     * for in release builds. Performance is not hindered by this attribute, but
     * it may be stripped from release builds. */
    std::string name;

  public:
    explicit Layer(std::string name = "Layer") noexcept;

    virtual ~Layer()          = default;
    Layer(const Layer& other) = default;
    Layer(Layer&& other)      = default;
    Layer& operator=(const Layer& other) = default;
    Layer& operator=(Layer&& other) = default;

    /** Invoked when layer is pushed onto the layer stack. */
    virtual void on_attach();

    /** Invoked when layer is popped (removed) from the layer stack. */
    virtual void on_detach();

    /** Invoked by the application when the application is updated. */
    virtual void on_update(Timestep timestep);

    /** Invoked by the application when the ImGui is rendered. */
    virtual void on_imgui_render();

    /** Invoked when an event gets sent to the layer. */
    virtual void on_event(Event& event);

    const std::string& get_name() const noexcept;
};

}

#endif
