#ifndef REAL_IMGUI_LAYER_HPP
#define REAL_IMGUI_LAYER_HPP

#include "core/Layer.hpp"
#include "core/Event.hpp"

namespace Real {

class ImGuiLayer : public Layer {
  private:
    float previous_time = 0.0f;

  public:
    ImGuiLayer();
    ~ImGuiLayer() override              = default;
    ImGuiLayer(const ImGuiLayer& other) = default;
    ImGuiLayer(ImGuiLayer&& other)      = default;
    ImGuiLayer& operator=(const ImGuiLayer& other) = delete;
    ImGuiLayer& operator=(ImGuiLayer&& other) = delete;

    void on_attach() override;
    void on_detach() override;

    void begin();
    void end();
};

}

#endif
