#ifndef REAL_REAL_HPP
#define REAL_REAL_HPP

/* -------------------- HEADER FILES USED BY APPLICATIONS ------------------- */
#include "core/Application.hpp"
#include "core/Log.hpp"

#include "core/Input.hpp"
#include "core/Key.hpp"
#include "core/MouseButton.hpp"

#include "imgui/ImGuiLayer.hpp"

#include "core/Timestep.hpp"

#include "renderer/Renderer.hpp"
#include "renderer/Renderer2D.hpp"
#include "renderer/RenderCommand.hpp"

#include "renderer/Buffer.hpp"
#include "renderer/Shader.hpp"
#include "renderer/Texture.hpp"
#include "renderer/VertexArray.hpp"

#include "renderer/OrthographicCamera.hpp"
#include "renderer/OrthographicCameraController.hpp"
/* -------------------------------------------------------------------------- */

#endif
