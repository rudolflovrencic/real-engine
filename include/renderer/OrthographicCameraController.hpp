#ifndef REAL_ORTHOGRAPHIC_CAMERA_CONTROLLER_HPP
#define REAL_ORTHOGRAPHIC_CAMERA_CONTROLLER_HPP

#include "renderer/OrthographicCamera.hpp"
#include "core/Event.hpp"
#include "core/Timestep.hpp"

namespace Real {

class OrthographicCameraController {
  private:
    constexpr static float camera_translation_speed = 3.0f;
    constexpr static float camera_rotation_speed    = 90.0f;

  private:
    float aspect_ratio;
    float zoom_level = 1.0f;
    OrthographicCamera camera;
    bool rotation;

    glm::vec3 camera_position = {0.0f, 0.0f, 0.0f};
    float camera_rotation     = 0.0f;

  public:
    /* Creates a camera with a zoom level of one meaning that vertically there
     * are two units of space. Horizontal space is calculated based on aspect
     * ratio. */
    explicit OrthographicCameraController(float aspect_ratio,
                                          bool rotation = false) noexcept;

    void on_update(Timestep timestep) noexcept;
    void on_event(Event& event) noexcept;

    OrthographicCamera& get_camera() { return camera; }
    const OrthographicCamera& get_camera() const { return camera; }

  private:
    void on_mouse_scroll(MouseScrolledEvent& event) noexcept;
    void on_window_resize(WindowResizeEvent& event) noexcept;
};

}

#endif
