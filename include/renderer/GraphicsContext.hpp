#ifndef REAL_GRAPHICS_CONTEXT_HPP
#define REAL_GRAPHICS_CONTEXT_HPP

namespace Real {

class GraphicsContext {
  public:
    virtual ~GraphicsContext()  = default;
    virtual void initialize()   = 0;
    virtual void swap_buffers() = 0;
};

}

#endif
