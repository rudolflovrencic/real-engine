#ifndef REAL_SHADER_HPP
#define REAL_SHADER_HPP

#include "core/Core.hpp"

#include <memory>
#include <unordered_map>
#include <filesystem>

#include <glm/glm.hpp>

namespace Real {

/** Encapsulation of a whole shader program that can run on a system GPU. */
class Shader {
  public:
    Shader()                    = default;
    virtual ~Shader()           = default;

    virtual void bind() const                   = 0;
    virtual void unbind() const                 = 0;
    virtual const std::string& get_name() const = 0;

    static std::shared_ptr<Shader> create(const std::filesystem::path& filepath);
    static std::shared_ptr<Shader> create(const std::string& name,
                              const std::string& vertex_src,
                              const std::string& fragment_src);

    /* Functions to expose for Real applications to use. This might be kept
     * until material system is in place (even then, it might be kept for
     * debugging). Expose more similar functions when you need them. */
    virtual void set_uniform_int(const std::string& name, int value) = 0;
    virtual void set_uniform_float3(const std::string& name,
                                    const glm::vec3& vec)            = 0;
    virtual void set_uniform_float4(const std::string& name,
                                    const glm::vec4& vec)            = 0;
    virtual void set_uniform_mat4(const std::string& name,
                                  const glm::mat4& mat)              = 0;
};

class ShaderLibrary {
  private:
    /** Mapping of shader names and shader objects. */
    std::unordered_map<std::string, std::shared_ptr<Shader>> shaders;

  public:
    /** Adds a shader to the library at the entry with the provided name. */
    void add(const std::string& name, const std::shared_ptr<Shader>& shader);

    /** Adds a shader to the library. Name of the entry is fetched from the
     * provided shader object. */
    void add(const std::shared_ptr<Shader>& shader);

    /** Loads a shader object from the provided file and adds it to the library.
     * Created shader object is returned. Name of the shader object is deduced
     * from the filename. */
    std::shared_ptr<Shader> load(const std::filesystem::path& filepath);

    /** Loads a shader object from the provided file and adds it to the library.
     * Created shader object is returned. Provided name is used to add the
     * shader to the library. */
    std::shared_ptr<Shader> load(const std::string& name,
                     const std::filesystem::path& filepath);

    /** Returns a shader from the library if the entry with such name exists. */
    std::shared_ptr<Shader> get(const std::string& name);

    /** Returns true if shader with the provided name exists in the library. */
    bool exists(const std::string& name) const;
};

}

#endif
