#ifndef REAL_RENDERER_API_HPP
#define REAL_RENDERER_API_HPP

#include <glm/glm.hpp>

#include "renderer/VertexArray.hpp"

namespace Real {

/** Abstraction of a platform specific rendering API. */
class RendererAPI {
  public:
    enum class API { None, OpenGL };

  private:
    static API api;

  public:
    virtual ~RendererAPI() = default;

    virtual void init() = 0;
    virtual void
    set_viewport(unsigned x, unsigned y, unsigned width, unsigned height) = 0;

    virtual void set_clear_color(const glm::vec4& color) = 0;
    virtual void clear()                                 = 0;

    virtual void draw_indexed(const VertexArray& vertex_array) = 0;

    static API get_api() noexcept;
};

}

#endif
