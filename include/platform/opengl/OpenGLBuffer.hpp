#ifndef REAL_OPENGL_BUFFER_HPP
#define REAL_OPENGL_BUFFER_HPP

#include "renderer/Buffer.hpp"

namespace Real {

class OpenGLVertexBuffer : public VertexBuffer {
  private:
    unsigned renderer_id;
    BufferLayout layout;

  public:
    OpenGLVertexBuffer(float* vertices, unsigned count) noexcept;
    ~OpenGLVertexBuffer() noexcept override;

    OpenGLVertexBuffer(const OpenGLVertexBuffer& other) = delete;
    OpenGLVertexBuffer& operator=(const OpenGLVertexBuffer& other) = delete;

    OpenGLVertexBuffer(OpenGLVertexBuffer&& other) noexcept;
    OpenGLVertexBuffer& operator=(OpenGLVertexBuffer&& other) noexcept;

    void set_layout(BufferLayout layout) noexcept override;
    const BufferLayout& get_layout() const noexcept override;

    void bind() const noexcept override;
    void unbind() const noexcept override;
};

class OpenGLIndexBuffer : public IndexBuffer {
  private:
    unsigned renderer_id;
    unsigned count;

  public:
    OpenGLIndexBuffer(unsigned* indices, unsigned count);
    ~OpenGLIndexBuffer() noexcept override;

    OpenGLIndexBuffer(const OpenGLIndexBuffer& other) = delete;
    OpenGLIndexBuffer& operator=(const OpenGLIndexBuffer& other) = delete;

    OpenGLIndexBuffer(OpenGLIndexBuffer&& other) noexcept;
    OpenGLIndexBuffer& operator=(OpenGLIndexBuffer&& other) noexcept;

    unsigned get_count() const noexcept override;

    void bind() const noexcept override;
    void unbind() const noexcept override;
};

}

#endif
