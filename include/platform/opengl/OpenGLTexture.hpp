#ifndef REAL_OPENGL_TEXTURE_HPP
#define REAL_OPENGL_TEXTURE_HPP

#include "renderer/Texture.hpp"

namespace Real {

class OpenGLTexture2D : public Texture2D {
  private:
    std::filesystem::path filepath;
    unsigned width;
    unsigned height;
    unsigned renderer_id;

  public:
    OpenGLTexture2D(std::filesystem::path filepath);
    ~OpenGLTexture2D() override;

    unsigned get_width() const override { return width; }
    unsigned get_height() const override { return height; };

    void bind(unsigned slot = 0) const override;
};

}

#endif
