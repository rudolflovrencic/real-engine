#ifndef REAL_OPENGL_CONTEXT_HPP
#define REAL_OPENGL_CONTEXT_HPP

#include "renderer/GraphicsContext.hpp"

struct GLFWwindow; // Forward declare GLFW window to avoid including GLFW.

namespace Real {

class OpenGLContext : public GraphicsContext {
  public:
    OpenGLContext(GLFWwindow* window);

    void initialize() override;
    void swap_buffers() override;

  private:
    GLFWwindow* window;
};

}

#endif
