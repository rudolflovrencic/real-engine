#ifndef REAL_OPENGL_RENDERER_API_HPP
#define REAL_OPENGL_RENDERER_API_HPP

#include "renderer/RendererAPI.hpp"

namespace Real {

class OpenGLRendererAPI : public RendererAPI {
  public:
    void init() noexcept override;
    void set_viewport(unsigned x,
                      unsigned y,
                      unsigned width,
                      unsigned height) noexcept override;

    void set_clear_color(const glm::vec4& color) noexcept override;
    void clear() noexcept override;

    void draw_indexed(const VertexArray& vertex_array) noexcept override;
};

}

#endif
