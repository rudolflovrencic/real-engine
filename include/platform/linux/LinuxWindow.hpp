#ifndef REAL_LINUX_WINDOW_HPP
#define REAL_LINUX_WINDOW_HPP

#include "core/Window.hpp"
#include "LinuxInput.hpp"
#include "renderer/GraphicsContext.hpp"

struct GLFWwindow; // Forward declare GLFW window to avoid including GLFW.

namespace Real {

class LinuxWindow : public Window {
  private:
    /** This class is owning a GLFW window, but this pointer cannot be unique
     * since the pointer is freed by using a special GLFW call. */
    GLFWwindow* window;

    struct WindowData {
        std::string title;
        unsigned width;
        unsigned height;
        bool vsync;
        EventCallback event_callback; ///< Invoked when an event occurs.
    };

    WindowData data;
    LinuxInput input;
    std::unique_ptr<GraphicsContext> context;

  public:
    LinuxWindow(const WindowProperties& props);
    ~LinuxWindow() override;

    LinuxWindow& operator=(const LinuxWindow& other) = delete;
    LinuxWindow(const LinuxWindow& other)            = delete;

    LinuxWindow(LinuxWindow&& other) noexcept;
    LinuxWindow& operator=(LinuxWindow&& other) noexcept;

    void on_update() override;

    unsigned get_width() const override;
    unsigned get_height() const override;

    void set_event_callback(EventCallback event_callback) override;

    void set_vsync(bool enabled) override;
    bool is_vsync() const override;

    void* get_native_window() const override;

  private:
    void initialize(const WindowProperties& props);
    void shutdown() noexcept;

    /** Sets callback functions of the GLFW library. Those functions create
     * appropriate event object (for example, key pressed event) and call the
     * event callback function in the window data member with the created event
     * object. */
    void set_glfw_callbacks() noexcept;
};

}

#endif
